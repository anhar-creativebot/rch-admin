<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li class="active">
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="./home.php">Dashboard</a>
        </li>
        <?php
        if (!stripos($_SERVER['REQUEST_URI'], 'home')){ ?>
          <li class="active"><a href="<?= $url ?>"><?= $breadcrumb_title; ?></a></li>
          <?php } ?>
        </ul>
      </div>

      <div class="page-content">
        <div class="page-header">
          <h1>
            <?= $breadcrumb_title; ?>

          </h1>
        </div>
