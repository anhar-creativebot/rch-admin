<?php header('Content-Type: application/javascript'); ?>
<script type="text/javascript">
$(document).ready(function(){$('#loginform').submit(function(evt){evt.preventDefault();$('.alert').hide();var username=$.trim($('#login-username').val());var password=$.trim($('#login-password').val());firebase.auth().signInWithEmailAndPassword(username,password).catch(function(error){$('#auth-incorrect').html('<strong>Error!</strong> Wrong email/password entered.<span class="close">&times;</span>').show();$('.close').click(function(){$('.alert').hide()})
$("#login-password").val('')})})})
</script>
