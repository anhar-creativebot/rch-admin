<?php header('Content-Type: application/javascript'); ?>
<script type="text/javascript">
$('#forgot-password').click(function(e){$('#resetChangedSuccess,#resetChangedDanger').html('');$('.alert').hide();e.preventDefault();$('#password_reset').modal({show:'true'})})
$('#reset').submit(function(evt){evt.preventDefault();var auth=firebase.auth();var emailAddress=$.trim($('#email').val());auth.sendPasswordResetEmail(emailAddress).then(function(){$('#resetChangedSuccess').html('Password reset email has been sent. Please check your email for further instructions.').show();setTimeout(function(){document.getElementById("reset").reset();$('#password_reset').modal('toggle')},2500)}).catch(function(error){$('#resetChangedDanger').html('Cannot send password reset email. Please try again later.').show()})})
</script>
