
<body class="no-skin">
	<div id="navbar" class="navbar navbar-default ace-save-state">
		<div class="navbar-container ace-save-state" id="navbar-container">
			<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
				<span class="sr-only">Toggle sidebar</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>

			<div class="navbar-header pull-left">
				<a href="./home.php" class="navbar-brand">
					<small>
						<i class="fa fa-book"></i>
						Research Centre for Hadith
					</small>
				</a>
			</div>

			<div class="navbar-buttons navbar-header pull-right" role="navigation">
				<ul class="nav ace-nav">
					<li class="light-blue dropdown-modal">
						<a data-toggle="dropdown" href="#" class="dropdown-toggle">
							<span class="user-info">
								<small>Welcome</small>
								<p>
									<script type="text/javascript">
									if(localStorage.getItem('user_authenticated') != null){
										$('span.user-info p').html(localStorage.getItem('user_authenticated'));
									}
									</script>
								</p>
							</span>
							<i class="ace-icon fa fa-caret-down"></i>
						</a>

						<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
							<li>
								<a href="settings.php">
									<i class="ace-icon fa fa-cog"></i>
									Profile Settings
								</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="#" id="logout">
									<i class="ace-icon fa fa-power-off"></i>
									Logout
								</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div><!-- /.navbar-container -->
	</div>

	<div class="main-container ace-save-state" id="main-container">
		<script type="text/javascript">
		try{ace.settings.loadState('main-container')}catch(e){}
		</script>

		<div id="sidebar" class="sidebar responsive ace-save-state">
			<script type="text/javascript">
			try{ace.settings.loadState('sidebar')}catch(e){}
			</script>

			<ul class="nav nav-list">
				<li class="<?php if ($active=='home') echo 'active';?>">
					<a href="./home.php">
						<i class="menu-icon fa fa-home"></i>
						<span class="menu-text"> RCH Dashboard </span>
					</a>
					<b class="arrow"></b>
				</li>

				<li class="<?php if ($active=='view-books') echo 'active';?>">
					<a href="view-books.php">
						<i class="menu-icon fa fa-book"></i>
						<span class="menu-text"> View Books </span>
					</a>
					<b class="arrow"></b>
				</li>

				<li class="<?php if ($active=='add-book') echo 'active';?>">
					<a href="add-book.php">
						<i class="menu-icon fa fa-plus-circle"></i>
						<span class="menu-text">
							Add Book
						</span>
					</a>
					<b class="arrow"></b>
				</li>

				<li class="<?php if ($active=='add-chapters') echo 'active';?>">
					<a href="add-chapters.php">
						<i class="menu-icon fa fa-plus-square-o"></i>
						<span class="menu-text"> Add Chapters </span>
					</a>

					<b class="arrow"></b>
				</li>

				<li class="<?php if ($active=='edit') echo 'active';?>">
					<a href="edit.php">
						<i class="menu-icon fa fa-pencil-square-o"></i>
						<span class="menu-text"> Edit </span>
					</a>
					<b class="arrow"></b>
				</li>

				<li class="<?php if ($active=='delete') echo 'active';?>">
					<a href="delete.php">
						<i class="menu-icon fa fa-trash"></i>
						<span class="menu-text"> Delete </span>
					</a>
					<b class="arrow"></b>
				</li>
			</ul>

			<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
				<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
			</div>
		</div>
