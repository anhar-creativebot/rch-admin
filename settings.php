<?php $title = 'RCH Dashboard/Profile Settings'; include_once('header.php'); ?>

<?php include_once('sidebar.php'); ?>

<?php $url=$_SERVER['REQUEST_URI']; $breadcrumb_title="Profile Settings"; include_once('breadcrumb.php'); ?>
<fieldset class="col-md-12">
	<legend><i class="fa fa-gear"></i> Profile Details</legend>
	<form id="settingsForm">
		<div class="row">
			<div class="col-md-6 col-sm-7 col-xs-12 form-group">
				<label for="display-name">Display Name:</label>
				<input type="text" class="form-control form-control-inline" type="text" required placeholder="Display Name" id="display-name" autocomplete="off">
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-sm-7 col-xs-12 form-group">
				<label for="email">Primary Email:</label>
				<input class="form-control form-control-inline" type="email" required placeholder="Email" id="email" autocomplete="off">
			</div>
		</div>
		<div class="row">
			<div class="alert alert-danger col-md-6 col-sm-7 col-xs-12" id="auth-incorrect" hidden></div>
			<div class="alert alert-success col-md-6 col-sm-7 col-xs-12" id="auth-correct" hidden></div>
		</div>
		<div class="">
			<button id="updateSettings" class="btn btn-info">Update Profile</button>
		</div>
	</form>
</fieldset>
<fieldset class="col-md-12">
	<legend><i class="fa fa-key"></i> Password Detail</legend>
	<form id="newPasswordSetting">
		<div class="row">
			<div class="col-md-6 col-sm-7 col-xs-12 form-group" style="margin-top:1em;">
				<div>
					<p>To change the password, please type in a new password</p>
				</div>
				<label for="new-password">New Password:</label>
				<input class="form-control form-control-inline" type="password" required placeholder="Password" id="new-password" autocomplete="off">
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-sm-7 col-xs-12 form-group">
				<label for="confirm-new-password">Confirm New Password:</label>
				<input class="form-control form-control-inline" type="password" required placeholder="Password" id="confirm-new-password" autocomplete="off">
			</div>
		</div>
		<div class="row">
			<div class="alert alert-success col-md-6 col-sm-7 col-xs-12" id="passwordChanged" hidden></div>
			<div class="alert alert-danger col-md-6 col-sm-7 col-xs-12" id="confirm-password-incorrect" hidden></div>
		</div>
		<div class="">
			<button id="updatePassword" class="btn btn-info">Update Password</button>
		</div>
	</form>
</fieldset>
</div>
</div>
</div>

<div id='password_modal' class="modal fade">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-center">Re-authenticatation Required</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="col-md-12 col-sm-12 col-xs-12 form-group" style="margin-top:1em;">
					<div class="row">
						<div class="text-center">
							<p>Please re-authenticate yourself by typing in your current password.</p>
						</div>
						<label for="authenticate-password">Current Password:</label>
						<input class="form-control form-control-inline" type="password" required placeholder="Password" id="authenticate-password" autocomplete="off">
						<div class="alert alert-success text-center" id="auth-password-success" hidden></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button id="authenticate" type="button" class="btn btn-info">Authenticate</button>
			</div>
		</div>
	</div>
</div>

<?php include_once('footer.php'); ?>
