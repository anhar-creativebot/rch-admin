<?php $title = 'RCH Dashboard/Add Book'; include_once('header.php'); ?>
<?php include_once('check-auth.php'); ?>

<?php $active="add-book"; include_once('sidebar.php'); ?>

<?php $url=$_SERVER['REQUEST_URI']; $breadcrumb_title="Add Book"; include_once('breadcrumb.php'); ?>
<form id="addBooksForm">
	<fieldset class="col-md-12">
		<legend><i class="fa fa-book"></i> Book Section</legend>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12 form-group">
			<label for="book-title">Book Title:</label>
			<input type="text" class="form-control form-control-inline" required placeholder="Book Title" id="book-title" autocomplete="off">
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12 form-group">
			<label for="book-description">Book Description:</label>
			<input type="text" class="form-control form-control-inline" required placeholder="Book Description" id="book-description" autocomplete="off">
		</div>
	</div><!-- /.row -->
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="row">
			<button id="bookSubmit" class="btn btn-info">Add Book</button>
		</div>
	</div>
	</fieldset>
</form>
</div>
</div>
</div>

<div id="confirm-add-book" class="modal fade">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Book added</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<?php include_once('footer.php'); ?>
<?php include_once('php-scripts/add-books.script.php'); ?>
