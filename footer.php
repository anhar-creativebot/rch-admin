<?php
if (!stripos($_SERVER['REQUEST_URI'], 'login')){ ?>
<div class="footer">
  <div class="footer-inner">
    <div class="footer-content">
      <span class="bigger-120">
        <span class="blue bolder">&copy; Research Center For Hadith</span>
        <?php echo date("Y"); ?>
      </span>
    </div>
  </div>
</div>

<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
  <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
</a>
</div>
<?php } ?>
</body>
</html>

<script type="text/javascript">
if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>

<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery-ui.custom.min.js"></script>
<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="assets/js/ace-elements.min.js"></script>
<script src="assets/js/ace.min.js"></script>
<?php
if (!stripos($_SERVER['REQUEST_URI'], 'login')){ ?>
<script>
$(document).ready(function () {
  $('#logout').click(function(e) {
    e.preventDefault();
    firebase.auth().signOut().then(function() {
      localStorage.removeItem('user_authenticated');
      window.location.replace('login.php');
    }, function(error) {
      console.error('Sign Out Error', error);
    });
  });
});
</script>
<?php } ?>
