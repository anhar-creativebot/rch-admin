<?php $title = 'RCH Dashboard/Edit'; include_once('header.php'); ?>
<?php include_once('check-auth.php'); ?>

<?php $active="edit"; include_once('sidebar.php'); ?>

<?php $url=$_SERVER['REQUEST_URI']; $breadcrumb_title="Edit"; include_once('breadcrumb.php'); ?>

<form id="update-book-form">
	<fieldset class="col-md-12">
		<legend><i class="fa fa-book"></i> Book Section</legend>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 form-group">
				<label for="book-title">Select Book:</label>
				<select class="form-control" id="select-book">
					<option value="default">
						Please select a book
					</option>
				</select>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 form-group" id="edit-book-name-input" hidden>
				<label for="edit-book-name">Edit Book Name:</label>
				<input type="text" class="form-control form-control-inline" type="text" required placeholder="Book Name" id="edit-book-name" autocomplete="off">
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 form-group" id="edit-book-description-input" hidden>
				<label for="edit-book-description">Edit Book Description:</label>
				<input type="text" class="form-control form-control-inline" type="text" required placeholder="Book Description" id="edit-book-description" autocomplete="off">
				<button id="update-book" class="btn btn-info" disabled>Update Book</button>
			</div>
			<div class="alert alert-success col-md-6 col-sm-6 col-xs-12" id="updateChanged" hidden></div>
		</div>
	</fieldset>
</form>
<form id="update-chapter-form">
	<fieldset class="col-md-12">
		<legend><i class="fa fa-book"></i> Chapter Section</legend>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 form-group">
				<label for="select-chapter">Select Chapter:</label>
				<select class="form-control" id="select-chapter" disabled>
					<option value="default">
						Please select a chapter
					</option>
				</select>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 form-group" id="edit-chapter-input" hidden>
				<label for="edit-chapter">Edit Chapter Title:</label>
				<input type="text" class="form-control form-control-inline" type="text" required placeholder="Chapter Title" id="edit-chapter" autocomplete="off">
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 form-group" id="chapter-content" hidden>
				<label for="chapter-body">Edit Chapter Content:</label>
				<textarea rows="20" class="form-control form-control-inline" required id="chapter-body"></textarea>
				<button id="update-chapter" class="btn btn-info" disabled>Update Chapter</button>
			</div>
			<div class="alert alert-success col-md-6 col-sm-6 col-xs-12" id="updateChapterChanged" hidden></div>
		</div>
	</fieldset>
</form>
</div>
</div>
</div>

<?php include_once('footer.php'); ?>
<?php include_once('php-scripts/edit.script.php'); ?>
