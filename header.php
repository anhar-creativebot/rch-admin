<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title><?= $title;  ?></title>

	<meta name="description" content="RCH Dashboard" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

	<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="assets/font-awesome/4.5.0/css/font-awesome.min.css" />

	<link rel="stylesheet" href="assets/css/fonts.googleapis.com.css" />
	<link rel="stylesheet" href="assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
	<link rel="stylesheet" href="assets/css/ace-skins.min.css" />
	<link rel="stylesheet" href="assets/css/style.min.css" />
	<link rel="stylesheet" href="assets/css/ace-rtl.min.css" />

	<script src="assets/js/ace-extra.min.js"></script>
	<script src="assets/js/jquery-2.1.4.min.js"></script>
	<script src="https://www.gstatic.com/firebasejs/3.6.7/firebase.js"></script>
	<script src="assets/js/firebase_config.min.js"></script>
	<script src="assets/js/modal.js"></script>

	<?php
	if (stripos($_SERVER['REQUEST_URI'], 'settings')){
		include_once('php-scripts/settings.script.php');
	}
	?>
</head>
