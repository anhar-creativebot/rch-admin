<?php $title = 'RCH Dashboard/View Books'; include_once('header.php'); ?>
<?php include_once('check-auth.php'); ?>

<?php $active="view-books"; include_once('sidebar.php'); ?>

<?php $url=$_SERVER['REQUEST_URI']; $breadcrumb_title="View Books"; include_once('breadcrumb.php'); ?>

<fieldset class="col-md-12">
	<legend><i class="fa fa-book"></i> Book Section</legend>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12 form-group">
			<label for="book-title">Select Book:</label>
			<select class="form-control" id="select-book">
				<option value="default">
					Please select a book
				</option>
			</select>
		</div>
	</div>
</fieldset>
<fieldset class="col-md-12">
	<legend><i class="fa fa-book"></i> Chapter Section</legend>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12 form-group">
			<label for="select-chapter">Select Chapter:</label>
			<select class="form-control" id="select-chapter" disabled>
				<option value="default">
					Please select a chapter
				</option>
			</select>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12 form-group" id="chapter-panel" hidden>
			<div class="panel panel-default">
				<div class="panel-heading text-center" id="chapter-panel-title"></div>
				<div class="panel-body" id="chapter-panel-body"></div>
			</div>
		</div>
	</div>
</fieldset>
</div>
</div>
</div>

<?php include_once('footer.php'); ?>
<?php include_once('php-scripts/view-books.script.php'); ?>
