<?php $title = 'RCH Dashboard/Add Chapters'; include_once('header.php'); ?>
<?php include_once('check-auth.php'); ?>

<?php $active="add-chapters"; include_once('sidebar.php'); ?>

<?php $url=$_SERVER['REQUEST_URI']; $breadcrumb_title="Add Chapters"; include_once('breadcrumb.php'); ?>

<fieldset class="col-md-12">
	<legend><i class="fa fa-book"></i> Book Section</legend>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12 form-group">
			<label for="book-title">Select Book:</label>
			<select class="form-control" id="select-book">
				<option value="default">
					Please select a book
				</option>
			</select>
			<div class="col-md-12 col-sm-12 col-xs-12 form-group" id="current-chapter-panel" hidden>
				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading" id="current-chapter-panel-title"></div>
						<div class="panel-body" id="current-chapter-panel-body"><ul></ul></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</fieldset>
		<form id="add-chapters">
			<fieldset class="col-md-12">
				<legend><i class="fa fa-book"></i> Chapter Section</legend>
				<label for="chapter-title">New Chapter Title:</label>
				<input type="text" class="form-control form-control-inline" required placeholder="Chapter Title" id="chapter-title" autocomplete="off">
				<label for="book-title">New Chapter Content:</label>
				<textarea rows="20" class="form-control form-control-inline" required placeholder="Content" id="chapter-content"></textarea>
				<button id="submit-chapters" class="btn btn-info" disabled>Add Chapter</button>
			</fieldset>
		</form>
</div>
</div>
</div>

<div id='add-chapters-modal' class="modal fade"></div>

<?php include_once('footer.php'); ?>
<?php include_once('php-scripts/add-chapters.script.php'); ?>
