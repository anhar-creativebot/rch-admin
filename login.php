<?php $title = 'RCH Dashboard/Login'; include_once('header.php'); ?>

<script>
var ref = firebaseConfig();
firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    $(document.body).hide();
    window.location ='home.php';
  }
});
</script>

<body>
  <div id="loginbox" class="container">
    <div class="mainbox col-md-6 col-lg-5 col-sm-6 col-xs-12">
      <div class="panel panel-info" >
        <div class="panel-heading">
          <div class="panel-title">Sign In</div>
          <div class="forgot-password-holder">
            <a id="forgot-password" href="#">Forgot password?</a>
          </div>
        </div>
        <div class="panel-body" >
          <div class="alert alert-danger" id="auth-incorrect" hidden></div>
          <form id="loginform" class="form-horizontal" role="form" method="post">
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
              <input id="login-username" type="email" class="form-control" name="username" value="" required placeholder="Email">
            </div>
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
              <input id="login-password" type="password" class="form-control" name="password" required placeholder="Password">
            </div>
            <div class="col-sm-12 controls">
              <div class="row">
                <button id="btn-login" class="btn btn-info">Login </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id='password_reset' class="modal fade">
    <form id="reset">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title text-center">Reset Password</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="col-md-12 col-sm-12 col-xs-12 form-group" style="margin-top:1em;">
              <div class="row">
                <p class="text-center">
                  Please type in your email address. We will send you an email to reset your password.
                </p>
                <label for="email">Email Address</label>
                <input type="email" class="form-control form-control-inline" required placeholder="joe@example.com" id="email" autocomplete="off">
                <div class="alert alert-success text-center" id="resetChangedSuccess" hidden></div>
                <div class="alert alert-danger text-center" id="resetChangedDanger" hidden></div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button id="reset-button" type="submit" class="btn btn-info">Send Email</button>
          </div>
        </div>
      </div>
    </form>
  </div>
  <?php include_once('footer.php'); ?>
  <?php include_once('php-scripts/login.script.php'); ?>
  <?php include_once('php-scripts/password-reset.script.php'); ?>
