<?php header('Content-Type: application/javascript'); ?>
<script type="text/javascript">
var ref = firebaseConfig();
firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    if(localStorage.getItem('user_authenticated') == null){
      let activeuser = firebase.auth().currentUser;
      localStorage.setItem('user_authenticated',activeuser.displayName);
      $('span.user-info p').html(activeuser.displayName);
    }else{
    }
  } else {
    $(document.body).hide();
    window.location = 'login.php';
  }
});
</script>
