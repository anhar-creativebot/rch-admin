<?php $title = 'RCH Dashboard/Delete'; include_once('header.php'); ?>
<?php include_once('check-auth.php'); ?>

<?php $active="delete"; include_once('sidebar.php'); ?>

<?php $url=$_SERVER['REQUEST_URI']; $breadcrumb_title="Delete"; include_once('breadcrumb.php'); ?>

<form id="delete-form">
  <fieldset class="col-md-12">
    <legend><i class="fa fa-book"></i> Book Section</legend>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 form-group">
        <label for="book-title">Select Book:</label>
        <select class="form-control" id="select-book">
          <option value="default">
            Please select a book
          </option>
        </select>
      </div>
      <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="delete-book-holder" hidden>
        <button id="delete-book" type="button" class="btn btn-info">Delete Book</button>
      </div>
      <div class="alert alert-success col-md-6 col-sm-6 col-xs-12" id="updateChanged" hidden></div>
    </div>
  </fieldset>
</form>
<fieldset class="col-md-12">
  <legend><i class="fa fa-book"></i> Chapter Section</legend>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
      <label for="select-chapter">Select Chapter:</label>
      <select class="form-control" id="select-chapter" disabled>
        <option value="default">
          Please select a chapter
        </option>
      </select>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="delete-chapter-holder" hidden>
      <button id="delete-chapter" type="button" class="btn btn-info">Delete Chapter</button>
    </div>
    <div class="alert alert-success col-md-6 col-sm-6 col-xs-12" id="updateChapterChanged" hidden></div>
  </div>
</fieldset>
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->

<div id='delete_book_confirm' class="modal fade">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center">Confirm Book Delete</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="col-md-12 col-sm-12 col-xs-12 form-group" style="margin-top:1em;">
          <div class="row">
            <div id="delete-body-text" class="text-center">
              <p></p>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button id="delete-book-yes" type="button" class="btn btn-info">Yes</button>
        <button id="delete-no" type="button" class="btn btn-info" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>

<div id='delete_chapter_confirm' class="modal fade">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center">Confirm Delete</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="col-md-12 col-sm-12 col-xs-12 form-group" style="margin-top:1em;">
          <div class="row">
            <div id="delete-body-text" class="text-center">
              <p></p>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button id="delete-chapter-yes" type="button" class="btn btn-info">Yes</button>
        <button id="delete-no" type="button" class="btn btn-info" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>

<?php include_once('footer.php'); ?>
<?php include_once('php-scripts/delete.script.php'); ?>
