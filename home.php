<?php $title = 'RCH Dashboard/Home'; include_once('header.php'); ?>
<?php include_once('check-auth.php'); ?>

<?php $active="home"; include_once('sidebar.php'); ?>

<?php $url=$_SERVER['REQUEST_URI']; $breadcrumb_title="Overview"; include_once('breadcrumb.php'); ?>
<script>
ref.once('value', function(snapshot) {
	$('#total-books').text(snapshot.numChildren())
});
</script>

<div class="row">
	<div class="col-md-5ths col-sm-4">
		<div class="wrimagecard wrimagecard-topimage">
			<a href="./view-books.php">
				<div class="wrimagecard-topimage_header">
					<center><i class="fa fa-book" style="color:#1664a0"></i></center>
				</div>
				<div class="wrimagecard-topimage_title">
					<h4>View Books
						<div class="pull-right badge hidden-xs" title="Total Books" id="total-books"></div></h4>
					</div>
				</a>
			</div>
		</div>
		<div class="col-md-5ths col-sm-4">
			<div class="wrimagecard wrimagecard-topimage">
				<a href="./add-book.php">
					<div class="wrimagecard-topimage_header">
						<center><i class="fa fa-plus-circle" style="color:#1664a0"></i></center>
					</div>
					<div class="wrimagecard-topimage_title">
						<h4>Add Book
							<div class="pull-right badge"></div></h4>
						</div>
					</a>
				</div>
			</div>
			<div class="col-md-5ths col-sm-4">
				<div class="wrimagecard wrimagecard-topimage">
					<a href="./add-chapters.php">
						<div class="wrimagecard-topimage_header">
							<center><i class="fa fa-plus-square-o" style="color:#1664a0"></i></center>
						</div>
						<div class="wrimagecard-topimage_title">
							<h4>Add Chapters
								<div class="pull-right badge" ></div></h4>
							</div>
						</a>
					</div>
				</div>
				<div class="col-md-5ths col-sm-4">
					<div class="wrimagecard wrimagecard-topimage">
						<a href="./edit.php">
							<div class="wrimagecard-topimage_header" >
								<center><i class="fa fa-pencil-square-o" style="color:#1664a0"></i></center>
							</div>
							<div class="wrimagecard-topimage_title">
								<h4>Edit
									<div class="pull-right badge" ></div></h4>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-5ths col-sm-4">
						<div class="wrimagecard wrimagecard-topimage">
							<a href="./delete.php">
								<div class="wrimagecard-topimage_header delete-card" >
									<center><i class="fa fa fa-trash" style="color:#d50f25"></i></center>
								</div>
								<div class="wrimagecard-topimage_title">
									<h4>Delete
										<div class="pull-right badge" ></div></h4>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php include_once('footer.php'); ?>
